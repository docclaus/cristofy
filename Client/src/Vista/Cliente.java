package Vista;

import Controlador.ControladorCliente;
import Controlador.codificadores_Cliente;
import java.awt.HeadlessException;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asus
 */
public class Cliente extends javax.swing.JFrame {

    /**
     * Creates new form Cliente
     */
    //Creas las columnas de la tabla con su información de la BD
    DefaultTableModel modelo = new DefaultTableModel();
        public String usuario;
        public ControladorCliente cliente;
        protected String idSelected;
        public codificadores_Cliente codificar = new codificadores_Cliente();
        public codificadores_Cliente decodificar = new codificadores_Cliente();

    public void setIdSelected(String idSelected) {
        this.idSelected = idSelected;
    }

    public void setCliente(ControladorCliente cliente) {
        this.cliente = cliente;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }   
    public Cliente(String login, ControladorCliente cliente) {
        
        initComponents();
        usuario = login;
        this.cliente= cliente;
        this.traeTabla();
        
        /*this.setVisible(true);*/
        //this.cargarTabla();
    }
    public Cliente(){}
                               
           

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jInternalFrame1 = new javax.swing.JInternalFrame();
        MenuPrincipal = new javax.swing.JInternalFrame();
        TablaGeneral = new javax.swing.JTabbedPane();
        tabCanciones = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        ListaCanciones = new javax.swing.JTable();
        Actualizar1 = new javax.swing.JButton();
        CierraSesion1 = new javax.swing.JButton();
        Bienvenida1 = new javax.swing.JLabel();
        tabFavoritos = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListaFavoritas = new javax.swing.JTable();
        Aniadir = new javax.swing.JPanel();
        NombreCancion = new javax.swing.JLabel();
        Ejemplo = new javax.swing.JTextField();
        Artista = new javax.swing.JLabel();
        Ejemplo2 = new javax.swing.JTextField();
        Duración = new javax.swing.JLabel();
        Ejemplo3 = new javax.swing.JTextField();
        mp3 = new javax.swing.JLabel();
        AniadirCancion = new javax.swing.JButton();
        rutaCancion = new javax.swing.JButton();
        Calidad = new javax.swing.JLabel();
        Ejemplo4 = new javax.swing.JTextField();
        Valoracion2 = new javax.swing.JLabel();
        Ejemplo5 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        log = new javax.swing.JPanel();
        Limpiar = new javax.swing.JButton();
        PanelConScroll = new javax.swing.JScrollPane();
        ventanaLog = new javax.swing.JTextArea();

        jInternalFrame1.setVisible(true);

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        MenuPrincipal.setVisible(true);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, MenuPrincipal, org.jdesktop.beansbinding.ELProperty.create("Menu Principal"), MenuPrincipal, org.jdesktop.beansbinding.BeanProperty.create("title"));
        bindingGroup.addBinding(binding);

        ListaCanciones.setAutoCreateRowSorter(true);
        ListaCanciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Titulo", "Artista", "Duracion", "Genero", "Valoracion", "Ruta", "Tam bytes", "Propietario", "Opciones", "Progresion"
            }
        ));
        ListaCanciones.setColumnSelectionAllowed(true);
        ListaCanciones.setSurrendersFocusOnKeystroke(true);
        ListaCanciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ListaCancionesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(ListaCanciones);

        Actualizar1.setText("Actualizar");
        Actualizar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Actualizar1MouseClicked(evt);
            }
        });
        Actualizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Actualizar1ActionPerformed(evt);
            }
        });

        CierraSesion1.setText("Cerrar sesión");
        CierraSesion1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CierraSesion1MouseClicked(evt);
            }
        });
        CierraSesion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CierraSesion1ActionPerformed(evt);
            }
        });

        Bienvenida1.setText("Bienvenido ");

        javax.swing.GroupLayout tabCancionesLayout = new javax.swing.GroupLayout(tabCanciones);
        tabCanciones.setLayout(tabCancionesLayout);
        tabCancionesLayout.setHorizontalGroup(
            tabCancionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabCancionesLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(Bienvenida1)
                .addGap(323, 323, 323)
                .addComponent(CierraSesion1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabCancionesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Actualizar1)
                .addGap(401, 401, 401))
        );
        tabCancionesLayout.setVerticalGroup(
            tabCancionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabCancionesLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(Actualizar1)
                .addGap(3, 3, 3)
                .addGroup(tabCancionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Bienvenida1)
                    .addComponent(CierraSesion1))
                .addGap(13, 13, 13))
        );

        TablaGeneral.addTab("Canciones", tabCanciones);

        ListaFavoritas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Titulo", "Artista", "Duracion", "Genero", "Valoracion", "Ruta", "Tam bytes", "Propietario", "Opciones"
            }
        ));
        ListaFavoritas.setEnabled(false);
        jScrollPane3.setViewportView(ListaFavoritas);

        javax.swing.GroupLayout tabFavoritosLayout = new javax.swing.GroupLayout(tabFavoritos);
        tabFavoritos.setLayout(tabFavoritosLayout);
        tabFavoritosLayout.setHorizontalGroup(
            tabFavoritosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE)
        );
        tabFavoritosLayout.setVerticalGroup(
            tabFavoritosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
        );

        TablaGeneral.addTab("Favoritas", tabFavoritos);

        NombreCancion.setText("Nombre de la canción:");

        Ejemplo.setForeground(new java.awt.Color(102, 102, 102));
        Ejemplo.setText("Ejemplo");
        Ejemplo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EjemploActionPerformed(evt);
            }
        });

        Artista.setText("Artista:");

        Ejemplo2.setForeground(new java.awt.Color(102, 102, 102));
        Ejemplo2.setText("Ejemplo");
        Ejemplo2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ejemplo2ActionPerformed(evt);
            }
        });

        Duración.setText("Duración:");

        Ejemplo3.setForeground(new java.awt.Color(102, 102, 102));
        Ejemplo3.setText("00:00");
        Ejemplo3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ejemplo3ActionPerformed(evt);
            }
        });

        mp3.setText(".mp3");

        AniadirCancion.setText("Añadir");
        AniadirCancion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AniadirCancionMouseClicked(evt);
            }
        });
        AniadirCancion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AniadirCancionActionPerformed(evt);
            }
        });

        rutaCancion.setText("...");
        rutaCancion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rutaCancionMouseClicked(evt);
            }
        });
        rutaCancion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rutaCancionActionPerformed(evt);
            }
        });

        Calidad.setText("Calidad:");

        Ejemplo4.setForeground(new java.awt.Color(102, 102, 102));
        Ejemplo4.setText("250kbs");
        Ejemplo4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ejemplo4ActionPerformed(evt);
            }
        });

        Valoracion2.setText("Valoración:");

        Ejemplo5.setForeground(new java.awt.Color(102, 102, 102));
        Ejemplo5.setText("Del uno al cinco.");
        Ejemplo5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ejemplo5ActionPerformed(evt);
            }
        });

        jLabel1.setText("Kbs");

        jLabel2.setText("Estrellas");

        jLabel3.setText("Min");

        javax.swing.GroupLayout AniadirLayout = new javax.swing.GroupLayout(Aniadir);
        Aniadir.setLayout(AniadirLayout);
        AniadirLayout.setHorizontalGroup(
            AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AniadirLayout.createSequentialGroup()
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AniadirLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addComponent(NombreCancion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Ejemplo, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addComponent(Artista)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Ejemplo2, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addComponent(Duración)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Ejemplo3, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addComponent(Calidad)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Ejemplo4, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addComponent(Valoracion2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Ejemplo5, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mp3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rutaCancion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel1))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2))
                            .addGroup(AniadirLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel3))))
                    .addGroup(AniadirLayout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(AniadirCancion)))
                .addContainerGap(331, Short.MAX_VALUE))
        );
        AniadirLayout.setVerticalGroup(
            AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AniadirLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NombreCancion)
                    .addComponent(Ejemplo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mp3)
                    .addComponent(rutaCancion))
                .addGap(26, 26, 26)
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Artista)
                    .addComponent(Ejemplo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Duración)
                    .addComponent(Ejemplo3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(28, 28, 28)
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Calidad)
                    .addComponent(Ejemplo4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(28, 28, 28)
                .addGroup(AniadirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valoracion2)
                    .addComponent(Ejemplo5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(41, 41, 41)
                .addComponent(AniadirCancion)
                .addContainerGap(103, Short.MAX_VALUE))
        );

        TablaGeneral.addTab("Añadir Canción", Aniadir);

        Limpiar.setText("Limpiar");
        Limpiar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LimpiarMouseClicked(evt);
            }
        });
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });

        ventanaLog.setEditable(false);
        ventanaLog.setColumns(20);
        ventanaLog.setRows(5);
        PanelConScroll.setViewportView(ventanaLog);

        javax.swing.GroupLayout logLayout = new javax.swing.GroupLayout(log);
        log.setLayout(logLayout);
        logLayout.setHorizontalGroup(
            logLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(Limpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 192, Short.MAX_VALUE)
                .addComponent(PanelConScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        logLayout.setVerticalGroup(
            logLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, logLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(logLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(logLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Limpiar))
                    .addComponent(PanelConScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE))
                .addContainerGap())
        );

        TablaGeneral.addTab("Log", log);

        javax.swing.GroupLayout MenuPrincipalLayout = new javax.swing.GroupLayout(MenuPrincipal.getContentPane());
        MenuPrincipal.getContentPane().setLayout(MenuPrincipalLayout);
        MenuPrincipalLayout.setHorizontalGroup(
            MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TablaGeneral, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        MenuPrincipalLayout.setVerticalGroup(
            MenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TablaGeneral)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MenuPrincipal)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MenuPrincipal)
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        
        
    }//GEN-LAST:event_LimpiarActionPerformed

    private void EjemploActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EjemploActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EjemploActionPerformed

    private void Ejemplo2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ejemplo2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Ejemplo2ActionPerformed

    private void Ejemplo3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ejemplo3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Ejemplo3ActionPerformed

    private void AniadirCancionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AniadirCancionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AniadirCancionActionPerformed

    private void AniadirCancionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AniadirCancionMouseClicked
        // TODO add your handling code here:
        /*
        this.Enviarmensajes("Intentando añadir canción");
        if (!(Ejemplo.getText().isEmpty() && Ejemplo2.getText().isEmpty() && Ejemplo3.getText().isEmpty())) {
            try {
                CancionesController CanControl = new CancionesController();
                this.Enviarmensajes("Creado un nuevo contructor, borrará el antiguo machacñandolo");
                CanControl.AñadirCanciones();
                this.Enviarmensajes("Canciones Añadidas");
                this.cargarTabla();
                this.Enviarmensajes("Actualizando...");
                this.ListaDeCanciones.setModel(modelo);
                this.ListaDeCanciones.updateUI();
                this.Enviarmensajes("Tabla Actualizada");
                
            } catch (SQLException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else
            this.Enviarmensajes("Faltan parámetros para añadir una canción");
            System.out.println("Faltan parámetros para Añadir una canción");
        */
    }//GEN-LAST:event_AniadirCancionMouseClicked

    private void Ejemplo4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ejemplo4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Ejemplo4ActionPerformed

    private void Ejemplo5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ejemplo5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Ejemplo5ActionPerformed

    private void LimpiarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LimpiarMouseClicked
        // TODO add your handling code here:
        this.ventanaLog.setText("");
    }//GEN-LAST:event_LimpiarMouseClicked

    private void rutaCancionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rutaCancionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rutaCancionActionPerformed

    private void rutaCancionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rutaCancionMouseClicked
        // TODO add your handling code here:
        this.Enviarmensajes("Importando cadena...");
        try {
            this.ImportarArchivo();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.Enviarmensajes("Cadena Importada...");
    }//GEN-LAST:event_rutaCancionMouseClicked

    private void Actualizar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Actualizar1MouseClicked
                // TODO add your handling code here:
        this.ListaCanciones.updateUI();
        this.Enviarmensajes("Lista Actualizada");
    }//GEN-LAST:event_Actualizar1MouseClicked

    private void Actualizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Actualizar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Actualizar1ActionPerformed

    private void CierraSesion1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CierraSesion1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_CierraSesion1MouseClicked

    private void CierraSesion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CierraSesion1ActionPerformed
              
         login lg = new login();
            this.setVisible(false);
            lg.setVisible(true);  
            System.out.println("Cierra la sesión actual");
            this.Enviarmensajes("Cerrando Sesión...");

    }//GEN-LAST:event_CierraSesion1ActionPerformed

    private void ListaCancionesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ListaCancionesMouseClicked
        // TODO add your handling code here:
        
     
         
            int column = ListaCanciones.getColumnModel().getColumnIndexAtX(evt.getX());
            int row = evt.getY()/ListaCanciones.getRowHeight();
            if (row < ListaCanciones.getRowCount() && row >= 0 && column < ListaCanciones.getColumnCount() && column >= 0){
                Object value = ListaCanciones.getValueAt(row, column);
                if (value instanceof JButton){
                    ((JButton)value).doClick();
                    JButton botonDescarga = (JButton) value;
                    try {
                        // Evento descargar.
                        // Envía al servidor una petición de descarga para el cliente identificado por login de la canción identificada por id;
                        cliente.Descargar(botonDescarga.getName());
                        
                    } catch (IOException ex) {
                        Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
 
        
    }//GEN-LAST:event_ListaCancionesMouseClicked
   
    public void Enviarmensajes(String cadena){
        
        this.ventanaLog.append(cadena + "\n");
        
        
    }
    
    public void traeTabla(){
    try {           
            System.out.println(usuario);
            String songs = this.cliente.traerCanciones(this.usuario);
            System.out.println(songs);
            if(!songs.isEmpty()){
                
                songs = songs.substring(28, songs.length()-4);
                
                String[] columns = { "id", "Titulo", "Artista", "Duracion", "Genero", "Valoracion", "Propietario", "Opciones", "Progresion" };
                
                String[] canciones = songs.split("#");
                DefaultTableModel modeloTodas = new DefaultTableModel(columns,0);
                ListaCanciones.setDefaultRenderer(Object.class, new ButtonRenderer());
                for (String cancione : canciones) {
                    
                    String[] aux = cancione.split("@");
                    JButton PLAY = new JButton("Play");
                   
                    Object[] datosFila = new Object[ListaCanciones.getColumnCount()];
                    datosFila[0] = aux[0];
                    datosFila[1] = aux[3];
                    datosFila[2] = aux[4];
                    datosFila[3] = aux[6];
                    datosFila[4] = aux[5];
                    datosFila[5] = aux[7];
                    /*datosFila[6] = aux[1];
                    datosFila[7] = aux[2];*/
                    PLAY.setName(aux[0].toString());
                    
                    if(aux[8].contains("TRUE")){
                        datosFila[6] = aux[8].substring(0, aux[8].length()-5);
                    }else if(aux[8].contains("FALSE")){
                        datosFila[6] = aux[8].substring(0, aux[8].length()-6);
                    }
                    datosFila[7] = PLAY;
                    modeloTodas.addRow(datosFila);
                }              
                ListaCanciones.setModel(modeloTodas);     
                
                DefaultTableModel modeloFavoritas = new DefaultTableModel(columns,0);
                ListaFavoritas.setDefaultRenderer(Object.class, new ButtonRenderer());
                for (String cancione : canciones) {
                    if(cancione.contains("TRUE")){
                        String[] aux = cancione.split("@");
                        JButton PLAY = new JButton("Play");
                        Object[] datosFila = new Object[ListaFavoritas.getColumnCount()];
                        datosFila[0] = aux[0];
                        datosFila[1] = aux[3];
                        datosFila[2] = aux[4];
                        datosFila[3] = aux[6];
                        datosFila[4] = aux[5];
                        datosFila[5] = aux[7];
                        /*datosFila[6] = aux[1];
                        datosFila[7] = aux[2];*/
                        
                        
                        if(aux[8].contains("TRUE")){
                            datosFila[6] = aux[8].substring(0, aux[8].length()-5);
                        }else if(aux[8].contains("FALSE")){
                            datosFila[6] = aux[8].substring(0, aux[8].length()-6);
                        }
                        datosFila[7] = PLAY;
                        modeloFavoritas.addRow(datosFila);
                    }
                }              
                ListaFavoritas.setModel(modeloFavoritas);
               
                /*
                ListaCanciones.getColumn("Opciones").setCellRenderer(new ButtonRenderer());
                ListaCanciones.getColumn("Opciones").setCellEditor(new ButtonEditor(new JCheckBox(),this.cliente));
                
                ListaFavoritas.getColumn("Opciones").setCellRenderer(new ButtonRenderer());
                ListaFavoritas.getColumn("Opciones").setCellEditor(new ButtonEditor(new JCheckBox(),this.cliente));
                */
                
                //ListaCanciones.getColumn("Progreso").setCellRenderer(new ProgressCellRender());
                //ListaFavoritas.getColumn("Progreso").setCellRenderer(new ProgressCellRender());
                                
                TableRowSorter<TableModel> elQueOrdena = new TableRowSorter<>(modeloTodas);
                ListaCanciones.setRowSorter(elQueOrdena);
                
                TableRowSorter<TableModel> elQueOrdena2 = new TableRowSorter<>(modeloFavoritas);
                ListaFavoritas.setRowSorter(elQueOrdena2);
            }
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void GuardarArchivo(){
    
         try {
                String nombre="";
                JFileChooser file=new JFileChooser();
                file.showSaveDialog(this);
                File guarda =file.getSelectedFile();
                //Indicamos lo que podemos seleccionar
                file.setFileSelectionMode(JFileChooser.FILES_ONLY);
                //Creamos el filtro
                FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.MP3", "mp3", "*.WAV", "wav");
                //Le indicamos el filtro
                file.setFileFilter(filtro);
  
                 if(guarda!=null){
                    //guardamos el archivo y le damos el formato directamente,
                    //podemos hacer que se guarde en formato mp3 lo definimos como .mp3 
                    try (FileWriter guardando = new FileWriter(guarda+".mp3")) {
                        guardando.write(Ejemplo.getText());
                    }
                    JOptionPane.showMessageDialog(null, "El archivo se a guardado Exitosamente", "Información",JOptionPane.INFORMATION_MESSAGE);
                }
         } catch(IOException ex){
            JOptionPane.showMessageDialog(null, "Su archivo no se ha guardado", "Advertencia",JOptionPane.WARNING_MESSAGE);
         }
    }


    public String ImportarArchivo() throws IOException{
    
        String aux="";   
        String texto="";
        try {
         //llamamos el metodo que permite cargar la ventana
         JFileChooser file = new JFileChooser();
         file.showOpenDialog(this);
         
         //abrimos el archivo seleccionado
         File abre = file.getSelectedFile();
            /*String abre = file.getSelectedFile().getName();*/
         //Indicamos lo que podemos seleccionar
         file.setFileSelectionMode(JFileChooser.FILES_ONLY);

        //Creamos el filtro
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.MP3", "mp3", "*.WAV", "wav");

        //Asociamos el filtro
        file.setFileFilter(filtro);

         //recorremos el archivo, lo leemos para plasmarlo en el area de texto 
         //debug
         System.out.println("Intentando abrir archivo...");
         if(abre!=null){   
             //debug
             System.out.println("archivo abierto...");
             //para seleccionar el nombre del archivo a devolver
             aux = file.getSelectedFile().getName();
             this.Ejemplo.setText("");
             this.Ejemplo2.setText("");

             
            //Abrimos y leemos el archivo  

             
             FileReader archivos = new FileReader(abre);
             
             try (BufferedReader leer = new BufferedReader(archivos)) {
                 while((texto=leer.readLine())!=null){
                     texto = aux + "\n";
                     //esto me ayuda a crear posiciones para pedirle en la ventana
                     //que me saque por dos salidas de objetos el cacho de string que me interesa
                     //para que contatene a partir de ahi
                     int pos=texto.indexOf('-');
                     int pos2=texto.indexOf('.');
                     
                     boolean resultado=texto.endsWith("-");
                     resultado=true;
                     

                     
                     if (!resultado){
                         //debug
                         //Si da el caso de que solo tiene el nombre de la canción sin el artista, asi que lo asigno a la caja de texto referente a titulo
                         System.out.println("Petando...");
                         String subCadena=texto.substring(0, pos2); //aqui peta
                         this.Ejemplo.setText(subCadena);
                         System.out.println("No peto...");
                         
                     }else{
                          //declaro el substring la posicion que me interesa, de un punto a otro
                            String subStr1=texto.substring(0, pos); 
                            this.Ejemplo2.setText(subStr1);

                            String subStr2=texto.substring(pos+1, pos2); 
                            this.Ejemplo.setText(subStr2);

                     }
                 }
                 this.Enviarmensajes("Titulo y Artista Importados.");
                 leer.close();
          
             } catch(IOException ex) {
            JOptionPane.showMessageDialog(null, ex + "" +
                  "\n No se ha encontrado el archivo",
                        "ADVERTENCIA!!!",JOptionPane.WARNING_MESSAGE);
          } 
        }
        } catch(FileNotFoundException exp) {
            JOptionPane.showMessageDialog(null, "El archivo no existe o no se" + "encuentra en esta carpeta.","Error", JOptionPane.WARNING_MESSAGE);
         } catch(HeadlessException exp) {
            System.out.println(exp);
           }

        return texto;//El texto se almacena en el JTextArea
}
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cliente().setVisible(true);
            }
        });
    }

    public JPanel getAniadir() {
        return Aniadir;
    }

    public JButton getAniadirCancion() {
        return AniadirCancion;
    }

    public JLabel getArtista() {
        return Artista;
    }

    public JPanel getCanciones() {
        return tabCanciones;
    }

    public JTextArea getConsolaLog() {
        return ventanaLog;
    }

    public JTextField getEjemplo() {
        return Ejemplo;
    }

    public JTextField getEjemplo2() {
        return Ejemplo2;
    }

    public JTextField getEjemplo3() {
        return Ejemplo3;
    }

    public JButton getLimpiar() {
        return Limpiar;
    }

    public JTable getListaDeCanciones() {
        return ListaCanciones;
    }

    public JLabel getNombreCancion() {
        return NombreCancion;
    }

    public JScrollPane getPanelConScroll() {
        return PanelConScroll;
    }

    public JTabbedPane getTablaGeneral() {
        return TablaGeneral;
    }

    public JLabel getValoracion() {
        return Duración;
    }

    public JInternalFrame getjInternalFrame1() {
        return jInternalFrame1;
    }

    public JInternalFrame getjInternalFrame2() {
        return MenuPrincipal;
    }

    public JScrollPane getjScrollPane2() {
        return jScrollPane2;
    }
public JScrollPane getjScrollPane3() {
        return jScrollPane3;
    }

    public JPanel getLog() {
        return log;
    }

    public JLabel getMp3() {
        return mp3;
    }

    public JButton getRutaCancion() {
        return rutaCancion;
    }

    public JButton getCierraSesion() {
        return CierraSesion1;
    }

    public void setCierraSesion(JButton CierraSesion) {
        this.CierraSesion1 = CierraSesion;
    }
/*    

     public static void limpiarTabla(){
            DefaultTableModel modelo=(DefaultTableModel) ClienteTabla.getModel();
            int filas=ClienteTabla.getRowCount();
            for (int i = 0;filas>i; i++) {
                modelo.removeRow(0);
            }
        }
*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Actualizar1;
    private javax.swing.JPanel Aniadir;
    private javax.swing.JButton AniadirCancion;
    private javax.swing.JLabel Artista;
    private javax.swing.JLabel Bienvenida1;
    private javax.swing.JLabel Calidad;
    private javax.swing.JButton CierraSesion1;
    private javax.swing.JLabel Duración;
    private javax.swing.JTextField Ejemplo;
    private javax.swing.JTextField Ejemplo2;
    private javax.swing.JTextField Ejemplo3;
    private javax.swing.JTextField Ejemplo4;
    private javax.swing.JTextField Ejemplo5;
    private javax.swing.JButton Limpiar;
    private javax.swing.JTable ListaCanciones;
    private javax.swing.JTable ListaFavoritas;
    private javax.swing.JInternalFrame MenuPrincipal;
    private javax.swing.JLabel NombreCancion;
    private javax.swing.JScrollPane PanelConScroll;
    private javax.swing.JTabbedPane TablaGeneral;
    private javax.swing.JLabel Valoracion2;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel log;
    private javax.swing.JLabel mp3;
    private javax.swing.JButton rutaCancion;
    private javax.swing.JPanel tabCanciones;
    private javax.swing.JPanel tabFavoritos;
    private javax.swing.JTextArea ventanaLog;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    public JLabel getBienvenida() {
        return Bienvenida1;
    }

    public void setBienvenida(JLabel Bienvenida) {
        this.Bienvenida1 = Bienvenida;
    }
}
