/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author lorian91
 */
public class ButtonRenderer extends DefaultTableCellRenderer {

    public ButtonRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
       
        if(value instanceof JButton){
            JButton btn = (JButton)value;
            btn.setText("PLAY");
            //btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/buttons/play.png")));
            return btn;
        }  
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
    
}
