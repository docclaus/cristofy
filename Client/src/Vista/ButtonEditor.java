/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorCliente;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;

/**
 *
 * @author lorian91
 */
public class ButtonEditor extends DefaultCellEditor {

    protected JButton button;
    private String label;
    private final ControladorCliente cliente;
    private boolean isPushed;
    private JTable table;

    public ButtonEditor(JCheckBox checkBox,ControladorCliente c) {
        super(checkBox);
        button = new JButton();
        button.setOpaque(true);
        button.addActionListener((ActionEvent e) -> {
            fireEditingStopped();
        }); 
        this.cliente = c;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (isSelected) {
            button.setForeground(table.getSelectionForeground());
            button.setBackground(table.getSelectionBackground());
        } else {
            button.setForeground(table.getForeground());
            button.setBackground(table.getBackground());
        }
        label = (value == null) ? "Play" : value.toString();
        button.setText(label);
        isPushed = true;
        this.table = table;
        return button;
    }

    @Override
    public Object getCellEditorValue() {
        if (isPushed) { 
            try {
                cliente.Descargar(this.table.getValueAt(this.table.getSelectedRow(), 0).toString());
            } catch (IOException ex) {
                Logger.getLogger(ButtonEditor.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        }
        isPushed = false;
        return label;
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }
}
