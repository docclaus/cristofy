package Vista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Controlador.ControladorCliente;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JOptionPane;


/**
 *
 * @author Asus
 */
public class login extends javax.swing.JFrame {
    
   ControladorCliente cl;
   Cliente cli;
   String login, pass;
    public login() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        VentanaInterna = new javax.swing.JInternalFrame();
        jPanel1 = new javax.swing.JPanel();
        InsertarNombreUsuario = new javax.swing.JTextField();
        InsertarContrasenia = new javax.swing.JPasswordField();
        BotonLogearse = new javax.swing.JButton();
        Usuario = new javax.swing.JLabel();
        Contraseña = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        NumPuerto = new javax.swing.JTextField();
        dirIp = new javax.swing.JTextField();
        Titulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        VentanaInterna.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        VentanaInterna.setVisible(true);

        InsertarNombreUsuario.setText("joseluis");
        InsertarNombreUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InsertarNombreUsuarioActionPerformed(evt);
            }
        });

        InsertarContrasenia.setText("joseluis");
        InsertarContrasenia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InsertarContraseniaActionPerformed(evt);
            }
        });

        BotonLogearse.setText("Logearse");
        BotonLogearse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BotonLogearseMouseClicked(evt);
            }
        });
        BotonLogearse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonLogearseActionPerformed(evt);
            }
        });

        Usuario.setText("Usuario");

        Contraseña.setText("Contraseña");

        jLabel1.setText("Puerto");

        jLabel2.setText("Dirección");

        NumPuerto.setText("5000");
        NumPuerto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumPuertoActionPerformed(evt);
            }
        });

        dirIp.setText("localhost");
        dirIp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dirIpActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(BotonLogearse)
                        .addGap(166, 166, 166))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Contraseña)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Usuario)
                                    .addComponent(jLabel2))
                                .addGap(21, 21, 21)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(NumPuerto)
                                    .addComponent(InsertarContrasenia)
                                    .addComponent(dirIp, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                                    .addComponent(InsertarNombreUsuario, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addComponent(jLabel1))
                        .addGap(93, 93, 93))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dirIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(NumPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(InsertarNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Usuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Contraseña)
                    .addComponent(InsertarContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addComponent(BotonLogearse)
                .addContainerGap())
        );

        Titulo.setFont(new java.awt.Font("Neogrey Regular", 1, 24)); // NOI18N
        Titulo.setText("BIENVENIDO A CRISTOFY");

        javax.swing.GroupLayout VentanaInternaLayout = new javax.swing.GroupLayout(VentanaInterna.getContentPane());
        VentanaInterna.getContentPane().setLayout(VentanaInternaLayout);
        VentanaInternaLayout.setHorizontalGroup(
            VentanaInternaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(VentanaInternaLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(Titulo)
                .addContainerGap(58, Short.MAX_VALUE))
        );
        VentanaInternaLayout.setVerticalGroup(
            VentanaInternaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, VentanaInternaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Titulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(VentanaInterna)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(VentanaInterna)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void InsertarNombreUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InsertarNombreUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InsertarNombreUsuarioActionPerformed

    private void InsertarContraseniaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InsertarContraseniaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InsertarContraseniaActionPerformed

    private void BotonLogearseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonLogearseMouseClicked
            
            login = InsertarNombreUsuario.getText().toString();
            char [] passChar =  InsertarContrasenia.getPassword();
                 try {
                     pass = conviertePassword(passChar);
                 } catch (SQLException ex) {
                     Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
                 }

                String host = dirIp.getText();
                int puerto = Integer.valueOf(NumPuerto.getText());
                cl = new ControladorCliente(host,puerto,login,pass);
                try {
                    if(cl.ComprobarCredenciales(login, pass)){
                        System.out.println("El login es correcto y el cliente lo sabe");
                        this.cambioVentana();
                       // cli.setUsuario(login);
                        
                        //cli.setCliente(cl); 
                    }              
                } catch (IOException ex) {
                    Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
                } catch (PropertyVetoException ex) {
                     Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
                 }
                
                


    }//GEN-LAST:event_BotonLogearseMouseClicked
    
    
    public String conviertePassword(char[] passwordChar) throws SQLException{
        String passwordString="";
        for(int i=0; i<passwordChar.length;i++){
            String aux=String.valueOf(passwordChar[i]);
            passwordString = passwordString+aux;
        }
        return passwordString;
    }
    public void cambioVentana() throws PropertyVetoException{
                        //Cerramos esta venta y abrimos otra
                        
                        /*this.getVentanaInterna().setClosed(true);*/

                        //creamos objeto ventana (DEBERIA HACER TODO ESTO EN OTRO MÉTODO)
                        
                        cli = new Cliente(login, cl);
                        //metodocreado para que los mensajes salgan en el log
                        cli.Enviarmensajes("Conectando con el servidor...");
                        cli.Enviarmensajes("Se ha conectado el usuario");
                        JOptionPane.showMessageDialog( null, "¡Bienvenido a Cristofy!\n\n Acepta para continuar." );
                        cli.Enviarmensajes("Esto es un log, todo lo que vaya realizando se registrará");
                        //version para consola de netbeans
                        System.out.println("Se ha conectado el usuario");
//                        cli.setVisible(true);
//                        this.VentanaInterna.removeAll();

                        try {
                            //lo borra todo directamente, no es eficiente si luego quiero usar esta ventana, asi que la ocultaré simplemente
                            /*this.getContentPane().removeAll(); //Quitamos todos los objeto del panel*/
                            
                            cli.setVisible(true); //añadimos la nueva ventana
                            this.dispose();
                            this.setVisible(false);
       

                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
    private void BotonLogearseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonLogearseActionPerformed
        // TODO add your handling code here:
        
        
        
        
    }//GEN-LAST:event_BotonLogearseActionPerformed

    private void NumPuertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NumPuertoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NumPuertoActionPerformed

    private void dirIpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dirIpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dirIpActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new login().setVisible(true);
            }
        });
    }

    public JButton getBotonLogearse() {
        return BotonLogearse;
    }

    public void setBotonLogearse(JButton BotonLogearse) {
        this.BotonLogearse = BotonLogearse;
    }

    public JLabel getContraseña() {
        return Contraseña;
    }

    public void setContraseña(JLabel Contraseña) {
        this.Contraseña = Contraseña;
    }

    public JPasswordField getInsertarContraseña() {
        return InsertarContrasenia;
    }

    public void setInsertarContraseña(JPasswordField InsertarContraseña) {
        this.InsertarContrasenia = InsertarContraseña;
    }

    public JTextField getInsertarNombreUsuario() {
        return InsertarNombreUsuario;
    }

    public void setInsertarNombreUsuario(JTextField InsertarNombreUsuario) {
        this.InsertarNombreUsuario = InsertarNombreUsuario;
    }

    public JLabel getTitulo() {
        return Titulo;
    }

    public void setTitulo(JLabel Titulo) {
        this.Titulo = Titulo;
    }

    public JLabel getUsuario() {
        return Usuario;
    }

    public void setUsuario(JLabel Usuario) {
        this.Usuario = Usuario;
    }

    public JInternalFrame getVentanaInterna() {
        return VentanaInterna;
    }

    public void setVentanaInterna(JInternalFrame VentanaInterna) {
        this.VentanaInterna = VentanaInterna;
    }

    public JPanel getjPanel1() {
        return jPanel1;
    }

    public void setjPanel1(JPanel jPanel1) {
        this.jPanel1 = jPanel1;
    }

    

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonLogearse;
    private javax.swing.JLabel Contraseña;
    private javax.swing.JPasswordField InsertarContrasenia;
    private javax.swing.JTextField InsertarNombreUsuario;
    private javax.swing.JTextField NumPuerto;
    private javax.swing.JLabel Titulo;
    private javax.swing.JLabel Usuario;
    private javax.swing.JInternalFrame VentanaInterna;
    private javax.swing.JTextField dirIp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables


}
