package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Usuario {
    protected String usuario;
    protected String password;
    protected boolean estado;
    protected String ip;
    
    public Usuario(){   
        this.usuario = "";
        this.password = "";
        this.estado = false;
        this.ip = "";
    }
    
    public Usuario(String usuario, String password,boolean estado, String ip){
        this.usuario = usuario;
        this.password = password;
        this.estado = estado;
        this.ip = ip;
    }
    
    //Sets
    public void setUsuario(String usuario){this.usuario = usuario;}
    public void setPassword(String password){this.password = password;}
    public void setEstado(boolean estado){this.estado = estado;}
    public void setIp(String ip){this.ip = ip;}
    //Gets 
    public String getUsuario(){return this.usuario;}
    public String getPassword(){return this.password;}
    public boolean getEstado(){return this.estado;}
    public String getIp(){return this.ip;}
    
    public String toString(){
        return this.getUsuario() + this.getEstado() + this.getIp();
    }
}

