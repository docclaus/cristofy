package modelo;

import java.util.ArrayList;

public class Lista_canciones {
    
    ArrayList<Cancion> listaCanciones = new ArrayList<Cancion>();
    String nombreCancion;
    String cadenaPeticion = null;

    public Lista_canciones() {
    }
    
    public ArrayList<Cancion> getListaCanciones() {
        return listaCanciones;
    }

    public void setListaCanciones(ArrayList<Cancion> listaCanciones) {
        this.listaCanciones = listaCanciones;
    }

    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    public String getCadenaPeticion() {
        return cadenaPeticion;
    }

    public void setCadenaPeticion(String cadenaPeticion) {
        this.cadenaPeticion = cadenaPeticion;
    }
    
    public String recuperarNombrePorId (String id){
        int i = 0;
        boolean encontrado = false;
        System.out.println("recupera "+listaCanciones);
        while(!encontrado){
            if (id.equals(this.listaCanciones.get(i).getId())){
                System.out.println("Buscamos por este id: "+id+" en la canción "+this.listaCanciones.get(i).getId());
                encontrado = true;
                nombreCancion = this.listaCanciones.get(i).getTitulo();
            }
            i++;
        }
        return nombreCancion;
    }
}
