/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author PC
 */
public class Cancion {
    String Id;
    String Titulo;
    String Artista;
    String Duracion;
    String Genero;
    String Valoracion;
    String Tamanio_bytes;
    String Ruta;
    String Id_usuario;
    boolean Favorita = false;

    public Cancion(String Titulo, String Artista, String Duracion, String Genero, String Valoracion, String Tamanio_bytes, String Ruta, String Id_usuario) {
        this.Titulo = Titulo;
        this.Artista = Artista;
        this.Duracion = Duracion;
        this.Genero = Genero;
        this.Valoracion = Valoracion;
        this.Tamanio_bytes = Tamanio_bytes;
        this.Ruta = Ruta;
        this.Id_usuario = Id_usuario;
    }
    
    public Cancion(){
        
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }
    
    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public String getArtista() {
        return Artista;
    }

    public void setArtista(String Artista) {
        this.Artista = Artista;
    }

    public String getDuracion() {
        return Duracion;
    }

    public void setDuracion(String Duracion) {
        this.Duracion = Duracion;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public String getValoracion() {
        return Valoracion;
    }

    public void setValoracion(String Valoracion) {
        this.Valoracion = Valoracion;
    }

    public String getTamanio_bytes() {
        return Tamanio_bytes;
    }

    public void setTamanio_bytes(String Tamanio_bytes) {
        this.Tamanio_bytes = Tamanio_bytes;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String Ruta) {
        this.Ruta = Ruta;
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Id_usuario;
    }

    public boolean getFavorita() {
        return Favorita;
    }

    public void setFavorita(boolean Favorita) {
        this.Favorita = Favorita;
    }
    
    @Override
    public String toString() {
        return "Cancion{" + "Id=" + Id + ", Titulo=" + Titulo + ", Artista=" + Artista + ", Duracion=" + Duracion + ", Genero=" + Genero + ", Valoracion=" + Valoracion + ", Tamanio_bytes=" + Tamanio_bytes + ", Ruta=" + Ruta + ", Favaorita=" + Favorita + ", Id_usuario=" + Id_usuario + '}';
    }
}