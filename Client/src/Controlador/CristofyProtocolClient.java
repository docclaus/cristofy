/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Cancion;
import modelo.Lista_canciones;

/**
 *
 * @author PC
 */
public class CristofyProtocolClient {
    
    
   
    

    


    ArrayList<String> MensajeParaCliente = new ArrayList<>();
    String user;
    String pass;
    String Ip;
    String UsuarioBD;
    String passBD;
    Boolean correcto;
    
    //jtext area para imprimir los mensajes al log del servidor
     public static javax.swing.JTextArea ventanaLog;
    //checkBox para validar ese log
     public static javax.swing.JCheckBox checking;
     String nombreHebra;
 


            
    public ArrayList<Cancion> PedirCanciones(String theInput){
        ArrayList Canciones = new ArrayList <Cancion>();
        StringTokenizer st = new StringTokenizer(theInput,"#");
        ArrayList <String> MensajeParaCliente = new ArrayList <String>();
        //para quitar y separar cada canción que ha traido del servidor al cliente
        while (st.hasMoreTokens()){
            MensajeParaCliente.add(st.nextToken());
        }
        //va borrando cada string del mensaje para luego asociarlo en un array temporal nuevo de forma local con la cadena más "limpia"
        MensajeParaCliente.remove((MensajeParaCliente.size())-1);
        MensajeParaCliente.remove(1);
        MensajeParaCliente.remove(0);
        for (int i = 0; i < MensajeParaCliente.size(); i++){
            Cancion aux = new Cancion();
            //desglosamos cada parte del "churro" con el arroba de los atributos de la canción con tokenizer por canción
            StringTokenizer tokenizerCancion = new StringTokenizer(MensajeParaCliente.get(i),"@");
            if (tokenizerCancion.hasMoreTokens()){
                aux.setId(tokenizerCancion.nextToken());
                aux.setRuta(tokenizerCancion.nextToken());
                aux.setTamanio_bytes(tokenizerCancion.nextToken());
                aux.setTitulo(tokenizerCancion.nextToken());
                aux.setArtista(tokenizerCancion.nextToken());
                aux.setGenero(tokenizerCancion.nextToken());
                aux.setDuracion(tokenizerCancion.nextToken());
                aux.setValoracion(tokenizerCancion.nextToken());
                aux.setId_usuario(tokenizerCancion.nextToken(":").substring(1));
                //en caso de ser la favorita del usuario, se envía el bool para que en la vista se incluya
                if (tokenizerCancion.nextToken().equalsIgnoreCase("TRUE")){
                    aux.setFavorita(true);
                }
            }
            //por cada vuelta, al final se agrega al array de la lista de las canciones que se encuentran en total.
            Canciones.add(aux);
        }
        Lista_canciones RecuperaCanciones = new Lista_canciones();
        RecuperaCanciones.setCadenaPeticion(theInput);
        return Canciones;

    }
    
    public ArrayList<Cancion> PedirFavoritas(String theInput){
        ArrayList favoritas = new ArrayList <Cancion>();
        StringTokenizer st = new StringTokenizer(theInput,"#");
        ArrayList <String> MensajeParaCliente = new ArrayList <String>();
        //para quitar y separar cada canción que ha traido del servidor al cliente
        while (st.hasMoreTokens()){
            MensajeParaCliente.add(st.nextToken());
        }
        //va borrando cada string del mensaje para luego asociarlo en un array temporal nuevo de forma local con la cadena más "limpia"
        MensajeParaCliente.remove((MensajeParaCliente.size())-1);
        MensajeParaCliente.remove(1);
        MensajeParaCliente.remove(0);
        for (int i = 0; i < MensajeParaCliente.size(); i++){
            Cancion aux = new Cancion();
            //desglosamos cada parte del "churro" con el arroba de los atributos de la canción con tokenizer por canción
            StringTokenizer tokenizerCancion = new StringTokenizer(MensajeParaCliente.get(i),"@");
            if (tokenizerCancion.hasMoreTokens()){
                aux.setId(tokenizerCancion.nextToken());
                aux.setRuta(tokenizerCancion.nextToken());
                aux.setTamanio_bytes(tokenizerCancion.nextToken());
                aux.setTitulo(tokenizerCancion.nextToken());
                aux.setArtista(tokenizerCancion.nextToken());
                aux.setGenero(tokenizerCancion.nextToken());
                aux.setDuracion(tokenizerCancion.nextToken());
                aux.setValoracion(tokenizerCancion.nextToken());
                aux.setId_usuario(tokenizerCancion.nextToken(":").substring(1));
                //en caso de ser la favorita del usuario, se envía el bool para que en la vista se incluya
                if (tokenizerCancion.nextToken().equalsIgnoreCase("TRUE")){//el equalsIgnoreCase ayuda a que permita pasar filtro ya sea en mayuscula o minuscula
                    aux.setFavorita(true);
                }
            }
            //en el caso de las favoritas, si tras el anterior if que se da el caso de que es una favorita, entonces se agrega, antes agregaba todo directamente
             if (aux.getFavorita()){
                favoritas.add(aux);
            }
            //por cada vuelta, al final se agrega al array de la lista de las canciones que se encuentran en total.
            favoritas.add(aux);
        }
        Lista_canciones RecuperaCanciones = new Lista_canciones();
        RecuperaCanciones.setCadenaPeticion(theInput);
        return favoritas;

    }
    
     public void descargarCancion(String id) throws IOException{
/*
        PrintWriter out = new PrintWriter(conexionCliente.getClientSocket().getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(conexionCliente.getClientSocket().getInputStream()));
        String clientLine = codificar.Base64DecodeString(in.readLine());
        System.out.println("\tAl protocolo, en primer lugar llega del server "+clientLine);
        st = new StringTokenizer(clientLine,"#");
        while (st.hasMoreTokens()){
            lineaClienteDividida.add(st.nextToken());
        }
        if (lineaClienteDividida.get(1).equals("RECEIVE")){
            //System.out.println("En el protocolo pediremos el nombre de la canción"+id+" :"+cancionesRecuperadas.getCadenaPeticion());
            //hebraDescarga = new ClientControlDownloadThread(id, conexionCliente);
            //hebraDescarga = new ClientDownloadThread(id, conexionCliente);
            //hebraDescarga.start();
        }*/
    }
    
    public ArrayList<String> mensajeServer(String cadena){
         ArrayList<String> MensajeCliente = new ArrayList<>();
         //uso del tokenizer, que divide los strings a partir del caracter que le limites
         StringTokenizer st = new StringTokenizer(cadena, "#");
             while (st.hasMoreTokens()) {
                    MensajeCliente.add(st.nextToken());
             }
    
        return MensajeCliente;
        
        }
}

    

