/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.Cliente;
import com.sun.javafx.application.PlatformImpl;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.JOptionPane;
import sun.misc.BASE64Encoder;

/**
 *
 * @author PC
 */
public final class ControladorCliente {
        
        String Nombre_Host;
        //variables necesarias del controlador
        int Puerto;
        Socket socket;
        private String id;
        private String usuario;
        codificadores_Cliente codificar = new codificadores_Cliente();
        //funciones de flujo y lectura de cadenas
        PrintWriter salida;
        BufferedReader entrada, stdIn;
        codificadores_Cliente descodificar = new codificadores_Cliente();
        //llamada al protocolo
        CristofyProtocolClient cp;

    public void setId(String id) {
        this.id = id;
    }
    
    //creación del constructor de mi controlador, donde deberé pasarle siempre
    //un host con su puerto para conectar con el servidor, y los credenciales
    // y además que contenga la creación y lanzador de socket de cliente-servidor con el protocolo
    public ControladorCliente(String host, int puerto, String user, String password) {
        this.Nombre_Host = host;
        this.Puerto = puerto;

        try {
            //socket que va a lanzar a través del puerto e IP del cliente
            this.socket = new Socket(this.Nombre_Host,this.Puerto);
            //Funcion que se utiliza para escribir en ficheros de texto, en este caso el mensaje que se mandará por el socket
            this.salida = new PrintWriter(this.socket.getOutputStream(), true);
            //te permiten hacer lecturas sencillas desde flujos de caracteres, hace lector del socket
            this.entrada = new BufferedReader( new InputStreamReader(this.socket.getInputStream()));
            
            //Lector
            this.stdIn = new BufferedReader(new InputStreamReader(System.in));
            // asocio y creo un nuevo protocolo cristofy de cliente
            cp = new CristofyProtocolClient();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Funcion que necesita pasar de la vista un usuario y contraseña, en la que comprobará
    //el logeado y enviará el siguiente mensaje al servidor para conectar
    public boolean ComprobarCredenciales(String us,String pass) throws UnsupportedEncodingException, IOException{
        System.out.println(us +" " +pass);
        System.out.println("Enviando mensaje:");
        String mensajeParaServidor = "PROTOCOLCRISTOFY1.0#CONNECT#"+us+"#"+pass;
        salida.println(codificar.Base64EncodeString(mensajeParaServidor));
        
        System.out.println(codificar.Base64EncodeString(mensajeParaServidor));
        System.out.println("cadena de string de salida DEL PRIMER CASO al servidor enviado!");
        
        //string que ayudará a la comprobación del mensaje llegado del server
        String fromServer;
        boolean logeado = false;
        
        //usuario a asociar 
        this.usuario = us;
        
        System.out.println("Antes de entrar a bucle por zona controlador cliente: " + this.entrada);
        
        //System.out.println(fromServer);
        //booleano que ayudará a que cuando lo que recibe del 
        //servidor es igual a la entrada leida del socket en el constructor siendo diferente de null
        //y que logeado sea diferente de true para salir
        
        
        
        fromServer = descodificar.Base64DecodeString(this.entrada.readLine());
        
        System.out.println("RESPUESTA DEL SERVIDOR: " + fromServer);
        if (fromServer.equals("PROTOCOLCRISTOFY1.0#OK#CONNECTED")) {
            System.out.println("El usuario está a la base de datos");
            logeado = true;
        }else{
            System.out.println("El usuario NO está en base de datos");
        }
        System.out.println("¿Llega?");
        //devuelve el estado para dar paso al siguiente estado
        return logeado;
    }
    
    //para cuando necesite canciones que esten asociadas al usuario
    public String traerCanciones(String user) throws IOException{
        System.out.println("y llegamos al caso 2: envío de petición de canciones para mostrar");
        String mensajeParaServidor = "PROTOCOLCRISTOFY1.0#FILES_USERS_SHARED#FAVORITES#"+user;
        
        
        //codificamos
        this.salida.println(codificar.Base64EncodeString(mensajeParaServidor));
        System.out.println(codificar.Base64EncodeString(mensajeParaServidor));
        System.out.println("cadena de string de salida DEL SEGUNDO CASO al servidor enviado!");
        
        String fromServer;
        boolean traidas = true;
            
         
        //si lo procedente del mensaje del server es igual a la entrada correspondiente y diferente de true procederá a traer canciones
        fromServer = descodificar.Base64DecodeString(this.entrada.readLine());
        System.out.println(fromServer);    
            
            System.out.println("Llegando canciones");
            
            System.out.println("Esperando las canciones pertinentes");
           
            
            
        
        return fromServer;
    }
    
    //tercera parte del protocolo, que una vez he obtenido las canciones, le pueda pedir que me las descargue
   
    public void Descargar(String id) throws IOException{
        
        System.out.println("y llegamos al caso3: PETICIÓN DE SELECCCIÓN Y DESCARGA!");
        System.out.println("Descargando cancion con id= "+id+" para el cliente: "+this.usuario);
        
        String mensajeParaServidor = "PROTOCOLCRISTOFY1.0#GET_FILE#"+id+"#"+this.usuario;

        codificar.Base64EncodeString(mensajeParaServidor);
        this.salida.println(codificar.Base64EncodeString(mensajeParaServidor));
        
        String fromServer;
        fromServer = descodificar.Base64DecodeString(this.entrada.readLine());
        boolean descargada = true;
        System.out.println(fromServer);
        
        //creamos fichero que se mandará la música transmitida
        File cancion = new File("music\\"+id+".mp3");
        ArrayList<byte[]> paquetes = new ArrayList<>();
        byte[] aux = null;
        FileOutputStream SalidaFichero = new FileOutputStream(cancion);
        
        
        System.out.println(usuario);
        while (!fromServer.equals("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+this.usuario+"#"+id+"#COMPLETED")) {
            
            System.out.println(fromServer);
            fromServer = descodificar.Base64DecodeString(this.entrada.readLine());
            System.out.println("Esperando Descargar...");
            aux=descodificar.Base64DecodeByte(cp.mensajeServer(fromServer).get(4));
            paquetes.add(aux);
                
        }
        //y así guardo el fichero
        for(int i=0; i<paquetes.size();i++){
            SalidaFichero.write(paquetes.get(i));
        }
        SalidaFichero.close();
        this.Reproducir(cancion);

    }

    public void Reproducir(File file){
        PlatformImpl.startup(() -> {});
        Media hit = new Media(file.toURI().toString()); 
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
  
    }

}