package Controlador;

import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;

public class codificadores_Cliente{
    public String Base64EncodeBytes(byte[] arrayBytes) throws UnsupportedEncodingException{
        byte[] encoded = Base64.encodeBase64(arrayBytes);
        return new String(encoded);
    }
    
    public String Base64EncodeString(String cadena) throws UnsupportedEncodingException{
        byte[] encoded = Base64.encodeBase64(cadena.getBytes());
        return new String(encoded);
    }
    
    public byte[] Base64DecodeByte(String cadena) throws UnsupportedEncodingException{
        byte[] decoded = Base64.decodeBase64(cadena.getBytes());      
        return decoded;
    }
    
    public String Base64DecodeString(String cadena) throws UnsupportedEncodingException{
        byte[] decoded = Base64.decodeBase64(cadena.getBytes());      
        return new String(decoded);
    }
}
