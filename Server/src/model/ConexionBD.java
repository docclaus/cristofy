/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import javax.sql.*;
import javax.naming.*;
/**
 *
 * @author Asus
 */
public class ConexionBD {
 
    static Connection con; // atributo para guardar el objeto conectado
    private static ConexionBD INSTANCE = null;
    
    public ArrayList<String> listaDatosUsuarios() throws SQLException {
        String seleccion = "SELECT Login FROM usuario"; // para insertar nombre de usuarios
        String seleccion2 = "SELECT Password FROM usuario"; //para insertar contraseña usuarios
        PreparedStatement ps = con.prepareStatement(seleccion);
        PreparedStatement ps2 = con.prepareStatement(seleccion2);
 
        ArrayList<String> ls = new ArrayList<String>(); //Creamos un arraylist para meter los datos extraídos de la tabla
 
        ResultSet rs = ps.executeQuery();
        ResultSet rs2 = ps2.executeQuery();
        con.commit();
        while(rs.next()){
            //datos de la tabla
            ls.add(rs.getString("Login"));// es la tupla de la base de datos
            ls.add(rs2.getString("Password"));
        }
        return ls;
        
        
    }
    
    public ArrayList<String> listaDatosCanciones() throws SQLException {
        
       
        String seleccion = "INSERT INTO cancion (Titulo, Artista, Duracion, Genero, Valoracion, Ruta, Tamanio_bytes, Id_usuario) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"; // para insertar todas las tuplas de canciones
     
        PreparedStatement ps = con.prepareStatement(seleccion);
 
 
        ArrayList<String> ls = new ArrayList<String>(); //Creamos un arraylist para meter los datos extraídos de la tabla
 
        ResultSet rs = ps.executeQuery();
        
        con.commit();
        while(rs.next()){
            //datos de la tabla
            ls.add(rs.getString("Titulo"));// es la tupla de la base de datos
            ls.add(rs.getString("Artista"));
            ls.add(rs.getString("Duracion"));
            ls.add(rs.getString("Genero"));
            ls.add(rs.getString("Valoracion"));
            ls.add(rs.getString("Ruta"));
            ls.add(rs.getString("Tamanio_bytes"));
            ls.add(rs.getString("Id_usuario"));
        }
        return ls;
        
        
    }
    
    
    /**Método constructor que ejecuta el método para conectar con la base de datos
     *
     */
    void ConexionBD() {
        performConnection();
    }
 
    //Crea una instancia de la base de datos en caso de no estar creada.

    private synchronized static void crearInstancia() {
        if (INSTANCE == null) {
            INSTANCE = new ConexionBD();
        }
    }
 
    //Metodo para devolver una instancia de la conexion. Si no esta creada la crea, y si esta creada la devuelve

    public static ConexionBD getInstancia() {
        if (INSTANCE == null) crearInstancia();
        return INSTANCE;
    }
 
    //Método para eliminar la instancia de la conexión

    public static void BorrarInstancia() {
        INSTANCE = null;
        CerrarConexion();
    }
 
    //Método que crea la conexión a la base de datos

    public static void performConnection() {
 
        try { 
                // preparamos la conexión 
                System.out.println("Estoy intentando conectarme a la base de datos");
                Class.forName("com.mysql.jdbc.Driver");
                Class.forName("org.gjt.mm.mysql.Driver");
            /*String newConnectionURL = "jdbc:mysql://" + host + "/" + dtbs + "?"
                    + "user=" + user + "&password=" + pass;*/
            //Conexion a la base de datos
            con = DriverManager.getConnection("jdbc:mysql://localhost/cristofy", "root", ""); //localhost
            System.out.println("Conexión establecida con Cristofy");
        } catch (Exception e) {
            System.out.println("Error al abrir la conexión.");
        }
    }
 
    /**Método para cerrar la conexión con la base de dades
     *
     */
    public static void CerrarConexion() {
        try {
            con.close();
            System.out.println("Base de Datos Cerrada");
        } catch (Exception e) {
            System.out.println("Error al cerrar la conexión.");
        }
    }


}

