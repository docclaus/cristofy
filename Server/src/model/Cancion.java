/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lorian91
 */
public class Cancion {
    private String id;
    private String titulo;
    private String artista;
    private String duracion;
    private String genero;
    private String valoracion;
    private String ruta;
    private String tam_bytes;
    private String id_usuario;

    public Cancion(String id, String titulo, String artista, String duracion, String genero, String valoracion, String ruta, String tam_bytes, String id_usuario) {
        this.id = id;
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;
        this.genero = genero;
        this.valoracion = valoracion;
        this.ruta = ruta;
        this.tam_bytes = tam_bytes;
        this.id_usuario = id_usuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTam_bytes() {
        return tam_bytes;
    }

    public void setTam_bytes(String tam_bytes) {
        this.tam_bytes = tam_bytes;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }
    

    
}
