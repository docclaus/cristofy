package model;

public class Usuario {
    protected String usuario;
    protected String password;
    
    public Usuario(){   
        this.usuario = "";
        this.password = "";

    }
    
    public Usuario(String usuario, String password){
        this.usuario = usuario;
        this.password = password;
 
    }
    
    //Sets
    public void setUsuario(String usuario){this.usuario = usuario;}
    public void setPassword(String password){this.password = password;}
    
    //Gets 
    public String getUsuario(){return this.usuario;}
    public String getPassword(){return this.password;}
   
    public String toString(){
        return this.getUsuario();
    }
}

