/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Biomecanica
 */
public class Favorito {
    String Id_usuario;
    String Id_cancion;

    public Favorito(String Id_usuario, String Id_cancion) {
        this.Id_usuario = Id_usuario;
        this.Id_cancion = Id_cancion;
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Id_usuario;
    }

    public String getId_cancion() {
        return Id_cancion;
    }

    public void setId_cancion(String Id_cancion) {
        this.Id_cancion = Id_cancion;
    }
}
