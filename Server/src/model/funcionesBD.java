package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

public class funcionesBD extends ConexionBD{
    
    protected String url;
    protected static Connection link;

    
    //METODO BUSCAR USUARIO EN BD

     
     public static ArrayList<Usuario> Buscar(String user,String pass) throws SQLException    {
        

        
        String consulta="SELECT login,password from cristofy.usuario";

        
        ArrayList <Usuario> listaUsuarios = new ArrayList <>();
                Statement stmt = null;
        ResultSet rs = null;
        try{
            performConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(consulta);
            
            while(rs.next()){
                Usuario aux= new Usuario(rs.getString("login"),rs.getString("password"));
                listaUsuarios.add(aux);
            }
        }
        catch (SQLException e ) {
            System.out.print(e);
        }finally{
      //finally block used to close resources
            try{
                if(stmt != null)
                    stmt.close();
            }catch(SQLException se){
            }// do nothing
            
        }//end try
        return listaUsuarios;
    }

     
     //METODOS CONEXIÓN BASE DE DATOS CANCIÓN
      public static ArrayList<Cancion> Buscar(String user) throws SQLException {
             
        String consulta="SELECT * from cancion";
        
        ArrayList <Cancion> listaCanciones = new ArrayList <>();
        Statement stmt = null;
        ResultSet rs = null;
        
        try{
            performConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(consulta);
            
            while(rs.next()){
                
                String id = rs.getString("Id");
                String titulo = rs.getString("Titulo"); 
                String artista = rs.getString("Artista");
                String duracion= rs.getString("Duracion");
                String genero= rs.getString("Genero");
                String valoracion= rs.getString("Valoracion");
                String ruta = rs.getString("Ruta");
                String tam_bytes = rs.getString("Tamanio_bytes");
                String id_usuario = rs.getString("Id_usuario");

                Cancion tmp = new Cancion(id,titulo,artista,duracion,genero,valoracion,ruta,tam_bytes,id_usuario); 
                listaCanciones.add(tmp);
            }
        }
        catch (SQLException e ) {System.out.print(e);}
        finally{
            try{if(stmt != null) stmt.close();}catch(SQLException se){ }
        }
        
        return listaCanciones;
    }
       public ArrayList<Favorito> BuscaFavoritos() throws SQLException{
           String consulta="SELECT * favoritos";
            ArrayList <Favorito> listaFavoritos = new ArrayList();
        
        Statement stmt = null;
        ResultSet rs = null;
       try{
            performConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(consulta);
            
            while(rs.next()){
            Favorito aux = new Favorito(rs.getString("Id_usuario"), rs.getString("Id_cancion"));
            listaFavoritos.add(aux);
            }
       }catch (SQLException e ) {System.out.print(e);}
        finally{
            try{if(stmt != null) stmt.close();}catch(SQLException se){ }
        }
        return listaFavoritos;
    }
    
   
    public Cancion Seleccionar(String idBusqueda) throws SQLException {
             
        String consulta="SELECT * from cancion where Id="+idBusqueda;
        
        Statement stmt = null;
        ResultSet rs = null;
        Cancion can= null;
        try{
      
            performConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(consulta);
            
            while(rs.next()){
                
                String id = rs.getString("Id");
                String titulo = rs.getString("Titulo"); 
                String artista = rs.getString("Artista");
                String duracion= rs.getString("Duracion");
                String genero= rs.getString("Genero");
                String valoracion= rs.getString("Valoracion");
                String ruta = rs.getString("Ruta");
                String tam_bytes = rs.getString("Tamanio_bytes");
                String id_usuario = rs.getString("Id_usuario");

                can = new Cancion(id,titulo,artista,duracion,genero,valoracion,ruta,tam_bytes,id_usuario); 
            }
        }
        catch (SQLException e ) {System.out.print(e);}
        finally{
            try{if(stmt != null) stmt.close();}catch(SQLException se){ }
        }
        return can;
    }
     
    public void desconexion(){

        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(funcionesBD.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error al desconectar");
        }
    }
}
