package ServerSocket;


import controlador.CancionController;
import controlador.UsuarioController;
import controlador.codificadores;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Cancion;
import model.Usuario;
import model.funcionesBD;


public class HebraConexionCliente extends Thread{
       private Socket socket = null;
       public String user_seed;
        public static String user_leech;
       //String cadena;
        
    String inServer, outServer;
    CristofyProtocolServer Cprotocolo;
    PrintWriter outCliente;
    BufferedReader inCliente;
    codificadores descodificar = new codificadores();
    codificadores codificar = new codificadores();   
       public static PrintWriter out_descarga = null;
       public static PrintWriter out_descarga1 = null;
            
    public HebraConexionCliente(Socket socket) {
    super("HebraConexionCliente");
    this.socket = socket;
    }
    
    public void run() {
        System.out.println("probando... entrando a try");   
    try {
        
        
         System.out.println("probando entradas");
        outCliente = new PrintWriter(socket.getOutputStream(), true);
        System.out.println("salida del socket al cliente");
        inCliente = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        System.out.println("leido el socket");
        vista.PanelServidor.ventanaLog.append("Leido socket" + "\n");
      
        //creamos el objeto protocolo para la comunicación de mensajes
       
        Cprotocolo = new CristofyProtocolServer();
        System.out.println("El cliente solicita conexión");
        //outServer = protocolo.processInput(""); //donde se le pasará el mensaje
        
        while ((inServer = inCliente.readLine()) != null) {
            //uso del metodo split, que separa cadenas de string en base a lo que le indiques creando vectores con ellas, en este caso el #

         vista.PanelServidor.ventanaLog.append(vista.PanelServidor.ventanaLog.getText().toString()+"Id hebra: " + this.getId() + " La IP: " + socket.getRemoteSocketAddress() + "\n" + "Manda esto al cliente: " + inServer + "\n");
          //outServer = protocolo.processInput(inServer);
          String descodificado;
          descodificado=descodificar.Base64DecodeString(inServer);
          Cprotocolo.mensajeCliente(descodificado);
          String accion = Cprotocolo.mensajeCliente(descodificado).get(1);
          
          //String decodedEntradaServer = new String(this.inServer.getBytes("UTF-8")); 
          //CASO 1, CUANDO LA ACCIÓN ES CONNECT y va a loguearse
          
          if (accion.equals("CONNECT")){
            String login = Cprotocolo.mensajeCliente(descodificado).get(2);
            String password = Cprotocolo.mensajeCliente(descodificado).get(3);
            String cadena =  Cprotocolo.ComprobarUsuario(login+"#"+password);
            vista.PanelServidor.ventanaLog.append(cadena);
            outCliente.println(cadena);
          }
          
          //CASO 2, CUANDO LA ACCIÓN ES QUE VOY A PEDIR CANCIONES
          if(accion.equals("FILES_USERS_SHARED")){
              String login=Cprotocolo.mensajeCliente(descodificado).get(3);
              System.out.println("ha recibido la petición de canciones de: " + login);
              vista.PanelServidor.ventanaLog.append("ha recibido la petición de canciones de: " + login);
               // El servidor devuelve al cliente la respuesta que obtiene a la petición de este último.
               vista.PanelServidor.ventanaLog.append(Cprotocolo.recuperarCanciones(login));
               System.out.println(Cprotocolo.recuperarCanciones(login));
               outCliente.println(Cprotocolo.recuperarCanciones(login));
            
          }
          // CASO 3. LA ACCIÓN ES SOLICITAR DESCARGA DE UN ARCHIVO
          if (accion.equals("GET_FILE")){
                        String login=Cprotocolo.mensajeCliente(descodificado).get(3);
                        String file=Cprotocolo.mensajeCliente(descodificado).get(2);
                        // El servidor devuelve al cliente la respuesta que obtiene a la petición.
                        System.out.println("El cliente pide descargar archivo");
                        vista.PanelServidor.ventanaLog.append("El cliente pide descargar archivo de id: ");
                        vista.PanelServidor.ventanaLog.append("RUTA DE LA CANCIÓN CON ID" + file + " y es: " + Cprotocolo.SolicitarCancion(file));
                        outCliente.println(codificar.Base64EncodeString("PROTOCOLCRISTOFY1.0#RECEIVE#"+file+"#PETITION_FROM"+login));
                        Cprotocolo.transmitirCancion(file, login, socket);
                        
    
          }
           
          }
        
        outCliente.close();
        inCliente.close();
        socket.close();
 
    } catch (IOException e) {
        e.printStackTrace();
    }      catch (SQLException ex) {
               Logger.getLogger(HebraConexionCliente.class.getName()).log(Level.SEVERE, null, ex);
           }

    }
    public boolean conectar(String user,String pass) throws SQLException{
        UsuarioController uc = new UsuarioController();
        return uc.comprobarUsuario(user, pass);
    }
    public void RecibirLeech(int indice) throws IOException{
        Socket nuevo_socket = SocketServer.array_clientes.get(indice).socket;
        out_descarga = new PrintWriter(nuevo_socket.getOutputStream(),true);
        //System.out.println("OUT DESCARGA: " + out_descarga);
    }
    
    public void SocketLeech(int indice) throws IOException{
        Socket nuevo_socket = SocketServer.array_clientes.get(indice).socket;
        out_descarga1 = new PrintWriter(nuevo_socket.getOutputStream(),true);
        //System.out.println("OUT DESCARGA: " + out_descarga1);
    }
    
        public String listaCanciones(String user) throws SQLException{
        
        CancionController cc = new CancionController();
        String canciones = cc.listar(user);
        
        System.out.println(canciones);
        
        return canciones;
    }

 public void enviarCancion(String id,String user) throws SQLException{
        CancionController cc = new CancionController();
        funcionesBD fBD = new funcionesBD();
        Cancion cancion = fBD.Seleccionar(id);
        
        BufferedInputStream bis;
        BufferedOutputStream bos;
        ByteArrayOutputStream theBOS;
        int in;
    
        byte[] byteArray;
        String msj = null;

        try {
            //File file = new File(cancion.getRuta());
            File file = new File("01 Turbo Lover.mp3");
            bis = new BufferedInputStream(new FileInputStream(file));
            theBOS = new ByteArrayOutputStream();

            //Enviamos el fichero
            byteArray = new byte[512];
   
            while( (in = bis.read(byteArray)) >= 0){
                msj = "PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+user+"#"+id+"#";
                theBOS.write(byteArray,0,in);
                
                msj = msj.concat(Base64.getEncoder().encodeToString(theBOS.toByteArray())); 
                System.out.println(msj);
                this.outCliente.println(msj);
            }
            System.out.println("Todo listo, enviando fichero");
            bis.close(); 
            theBOS.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HebraConexionCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HebraConexionCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
