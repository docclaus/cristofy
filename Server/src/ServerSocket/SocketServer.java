package ServerSocket;

import vista.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

public class SocketServer extends Thread{
    
     // Solo necesita saber el puerto que se le pasará para lanzar el socket
     int puerto;
     boolean listening = true;//boleano que ayuda con el while a que el serversocket esté escuchando peticiones
        //array para añadir peticiones de los clientes necesarios por la hebra
     ServerSocket serverSocket = null;//un servidor socket que pueda oirte
     public static ArrayList<HebraConexionCliente> array_clientes = new ArrayList<HebraConexionCliente>();
     HebraConexionCliente socket_hebra;
     
     public boolean isListening() {
        return this.listening;
    }

    public void setListening(boolean listening) {
        this.listening = listening;
    }
    
 public void parandohebra(){
        listening=false;
    }
 
 
    //constructor

    public SocketServer(ServerSocket serv, int port) throws IOException{
        super("SocketServer");
        puerto = port;
        serverSocket = serv;
        
    }
    public void run(){
        
         System.out.println("HOLA HOLITA");
        vista.PanelServidor.ventanaLog.append("INICIANDO SOCKET DE SERVIDOR" + "\n");
        int id_hebra_cliente=0;
        while (listening){
         vista.PanelServidor.ventanaLog.append("Se ha conectado correctamente el socket" + "\n");
         System.out.println("Me dispongo a escuchar");
            System.out.println("ESTOY ESCUCHANDO INFINITAMENTE");
                //if de mensajes y casilla
                vista.PanelServidor.ventanaLog.append("En espera de lanzar hebra" + "\n");
         
              try {
                  //crea socket con el cliente
                  //sincronizar para ejecución en exclusion mutua
                  
                  socket_hebra = new HebraConexionCliente(serverSocket.accept());
                  System.out.println("creado la hebra de conexion a cliente");
              } catch (IOException ex) {
                  Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
              }
                array_clientes.add(socket_hebra);
                vista.PanelServidor.ventanaLog.append("AÑADO AL ARRAY Y ACEPTO PETICIÓN DE CLIENTE" + "\n");
         
                //lanzo hebra de la lista la última
                array_clientes.get(id_hebra_cliente).start();
                id_hebra_cliente++;
                //socket_hebra.start();
               
        }
 /*
         try {
             serverSocket.close();
         } catch (IOException ex) {
             Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
         }*/
    }
    
    public void stopServer(){
        this.listening = false;
        try {
            System.out.println("Server stopped");
            //this.log.setLogInfo("Server stopped");
            serverSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static int buscarUsuarioSocket(String usuario){
       /*
        for(int j = 0; j < SocketServer.array_sockets.size();j++){
             if(SocketServer.array_sockets.get(j).getName().equals("HebraConexion")){
                 SocketServer.array_sockets.remove(j);
             }
        }  
*/
        for(int i = 0; i < SocketServer.array_clientes.size();i++){
             if(SocketServer.array_clientes.get(i).getName().equals(usuario)){
                 System.out.println("Encontrado cliente!!!!!!!!!!!!!!!!!!!!" + i);
                 return i;
             }
        }         
         return -1;      
    }

}
