package ServerSocket;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import controlador.CancionController;
import controlador.UsuarioController;
import controlador.codificadores;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.funcionesBD;
import vista.PanelServidor;

public class CristofyProtocolServer {

    
    String user;
    String pass;
    Boolean correcto;
    String nombreHebra;
    codificadores codificar = new codificadores(); 
    codificadores descodificar = new codificadores();

    public String ComprobarUsuario(String theInput) throws SQLException, UnsupportedEncodingException {
        PanelServidor.ventanaLog.append("Habiendo entrado en el protocolo, el mensaje sigue así: " + theInput + "\n");
         //Mensaje que envia el servidor a cliente
        String theOutput = null;
        
                //correcto = false;
                System.out.println(theInput);
                PanelServidor.ventanaLog.append("Habiendo entrado en el protocolo, el mensaje sigue así: " + theInput + "\n");
                    if(theInput.isEmpty()){
                         PanelServidor.ventanaLog.append(nombreHebra + "El cliente no envio nada aún\n");
                    } else{
                         
                        System.out.println("Y el mensaje que sale es... " + theInput);
                        System.out.println("Prueba de que tokenizer hace bien su trabajo: " + mensajeCliente(theInput).get(0) + mensajeCliente(theInput).get(1));
                        
                       /* if((theInput.equals("CONNECT")){*/
                           
                           System.out.println("Si he entrado aquí, es porque HE RECIBIDO UNA PETICIÓN DEL CLIENTE");
                           PanelServidor.ventanaLog.append("Petición de conexión usuario por parte de: " + mensajeCliente(theInput).get(0));
                                   
                           UsuarioController UC = new UsuarioController();
                           
                            correcto=UC.comprobarUsuario(mensajeCliente(theInput).get(0),mensajeCliente(theInput).get(1)); 
                           //correcto = UC.logearse(MensajeParaCliente.get(2), MensajeParaCliente.get(3));
                            System.out.println("El usuario pertenece a la base de datos? "+correcto);
                           if(correcto){
                               theOutput = "PROTOCOLCRISTOFY1.0#OK#CONNECTED";
                               PanelServidor.ventanaLog.append("PROTOCOLCRISTOFY1.0#OK#CONNECTED\n");
                           } else{
                               theOutput = "PROTOCOLCRISTOFY1.0#ERROR#INVALID_CREDENTIALS";
                               PanelServidor.ventanaLog.append("PROTOCOLCRISTOFY1.0#ERROR#INVALID_CREDENTIALS\n");
                           }
                              
                        /*} else{
                            PanelServidor.ventanaLog.append("PROTOCOLCRISTOFY1.0#INVALID_PROTOCOL\n");
                            return theOutput; 
                        }  */
                           
                    }
                    
        return codificar.Base64EncodeString(theOutput);
    }
    
    //esta función se encarga de recuperar todas las canciones traidas por el usuario a traves del ID
     public String recuperarCanciones(String id_user) throws SQLException, UnsupportedEncodingException{
        //llamo a mi respectivo controlador
         CancionController CC = new CancionController();
         //CC.listar(id_user);    
        return codificar.Base64EncodeString(CC.listar(id_user));
    }
    
     
    public ArrayList<String> mensajeCliente(String cadena){
         ArrayList<String> MensajeCliente = new ArrayList<>();
         //uso del tokenizer, que divide los strings a partir del caracter que le limites
         StringTokenizer st = new StringTokenizer(cadena, "#");
             while (st.hasMoreTokens()) {
                    MensajeCliente.add(st.nextToken());
             }
    
        return MensajeCliente;
        
        }
    
    
    public String SolicitarCancion (String id_cancion) throws SQLException, UnsupportedEncodingException{
        funcionesBD cancionID = new funcionesBD(); 
        return codificar.Base64EncodeString(cancionID.Seleccionar(id_cancion).getRuta());
        
    }
    
    public void transmitirCancion(String id, String login, Socket socket) throws SQLException, IOException, UnsupportedEncodingException{
       
        System.out.println(id);
        System.out.println(descodificar.Base64DecodeString(this.SolicitarCancion(id)));
        File file = new File(descodificar.Base64DecodeString(this.SolicitarCancion(id)));
        byte[] datos = new byte [(int) file.length()]; //creamos vector de bytes enteros
        FileInputStream fileIN = new FileInputStream(file);
        fileIN.read(datos); // fichero y lo guarda en el vector
        fileIN.close(); // cierra
        int num_paquetes = datos.length/512;
        int pos = 0;        
        byte[] aux=null; //creamos vector auxiliar que se usa para transmitir 
        PrintWriter outCliente = new PrintWriter(socket.getOutputStream(), true);
        System.out.println("Paquetes necesarios: " + ((file.length()/512)+1));
        System.out.println("Paquetes necesarios: " + num_paquetes);
        int contador=1;
        for(int i=0; i<num_paquetes;i++){ //recorre el numero de paquetes que se deben llenar
            aux = new byte[512];
            System.out.println(pos);
            
               for (int j=0; j<512;j++){ //tamaño envio e incremento de posicion para recorrer de todos los paquetes del original
                aux[j]=datos[pos];
                pos++;
                
               }
                System.out.println("Contador de paquete nº: " + contador + " " + "PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#"+codificar.Base64EncodeBytes(aux));
                outCliente.println(codificar.Base64EncodeString("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#"+codificar.Base64EncodeBytes(aux)));  
                contador++;
        }
        aux= new byte[datos.length-pos];
        System.out.println("BYTES TOTALES: " +datos.length);
        System.out.println("BYTES RESTANTES DEL ÚLTIMO PAQUETE: " + (datos.length-pos));
        for(int i=pos; i<aux.length;i++){
            aux[i]=datos[i];
        }
          
        outCliente.println(codificar.Base64EncodeString("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#"+codificar.Base64EncodeBytes(aux)));
        System.out.println("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#"+codificar.Base64EncodeBytes(aux));
        outCliente.println(codificar.Base64EncodeString("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#COMPLETED"));
        System.out.println("PROTOCOLCRISTOFY1.0#TRANSMISION_TO#"+login+"#"+id+"#COMPLETED");
        }
        
    
}
