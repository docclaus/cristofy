/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Cancion;
import model.funcionesBD;
import vista.PanelServidor;



public class CancionController {
    public String listar(String user) throws SQLException{
        
        ArrayList<Cancion> canciones = funcionesBD.Buscar(user);
        
        String stringCanciones= "PROTOCOLCRISTOFY1.0#OKSTART#";
        
        for(int i=0;i<canciones.size();i++){
            
            String id = canciones.get(i).getId();
            String titulo =  canciones.get(i).getTitulo(); 
            String artista =  canciones.get(i).getArtista();
            String duracion=  canciones.get(i).getDuracion();
            String genero=  canciones.get(i).getGenero();
            String valoracion=  canciones.get(i).getValoracion();
            String ruta =  canciones.get(i).getRuta();
            String tam_bytes =  canciones.get(i).getTam_bytes();
            String id_usuario =  canciones.get(i).getId_usuario();
            //ayudará a guardar el churro de la cadena para luego depositarlo en el cliente
            String aux = id+"@"+ruta+"@"+tam_bytes+"@"+titulo+"@"+artista+"@"+genero+"@"+duracion+"@"+valoracion+"@"+id_usuario+":";
            
                    
            if(id_usuario.equals(user)){
                System.out.println("Esta canción: "+canciones.get(i).getTitulo()+" es favorita de:  "+user);
                PanelServidor.ventanaLog.append("Esta canción: "+canciones.get(i).getTitulo()+" es favorita de:  "+user);
                aux+="TRUE#";
            }
            else {
                aux+="FALSE#";
            }
            System.out.println(aux);
            stringCanciones+=aux;
            System.out.println(aux);
        }
        stringCanciones+="END";
        
        return stringCanciones;
    }
    

}
