-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2017 a las 07:57:29
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cristofy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cancion`
--

CREATE TABLE `cancion` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Artista` varchar(40) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Duracion` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Genero` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Valoracion` double NOT NULL,
  `Ruta` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Tamanio_bytes` int(25) NOT NULL,
  `Id_usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cancion`
--

INSERT INTO `cancion` (`Id`, `Titulo`, `Artista`, `Duracion`, `Genero`, `Valoracion`, `Ruta`, `Tamanio_bytes`, `Id_usuario`) VALUES
(1, 'Roll Over Beethoven', 'Chuck Berry', '2', 'Classic Rock', 0, 'music/lolo/Roll Over Beethoven.mp3', 2269694, 'lolo'),
(2, 'Kings Never Die', 'Eminem', '5', 'Hip-Hop', 9, 'music/josed/eminem-kingsneverdie.mp3', 12303963, 'josed'),
(3, 'Smells Like Teen Spirit', 'Nirvana', '5', 'Grunge', 10, 'music/josed/nirvana-smellsliketeenspirit.mp3', 4811013, 'josed'),
(4, 'Kyoto', 'Skrillex', '3', 'Dubstep', 9, 'music/josed/skrillex-kyoto.mp3', 8139309, 'josed'),
(5, 'Corazón Indomable', 'Camela', '4:40', 'Pop?', 10, 'music/kiki/Corazon Indomable.mp3', 4485120, 'kiki'),
(6, 'Fade out lines', 'The Avener, Phoebe Killdeer', '4', 'Indie', 8, 'music/adil/The Avener - Fade Out Lines.mp3', 7798784, 'adil'),
(7, 'Thunderstruck', '2CELLOS', '5', 'clasica?', 4, 'music/adil/2CELLOS - Thunderstruck.mp3', 4186112, 'adil'),
(8, 'LP', 'Laura Pergolizzi', '4', 'House', 4, 'music/francis/LP.mp3', 9444618, 'francis'),
(10, 'Respect', 'Aretha Franklin', '2', 'RnB', 0, 'music/lolo/Respect.mp3', 2339336, 'lolo'),
(11, 'Chickies trombone', 'Joe Bataan', '2', 'Doo-wop', 0, 'music/lolo/Chickies trombone.mp3', 1592291, 'lolo'),
(12, 'Bonito y Sabroso', 'Benny More', '2:55', 'Mambo', 7, 'music/kiki/Bonito Y Sabroso.mp3', 2818048, 'kiki'),
(13, 'Si Vas', 'Rayden', '3', 'Hip Hop/Rap', 10, 'music/vilchez/Si vas.mp3', 3262170, 'vilchez'),
(14, ' Aceite En La Cintura', 'Mark B  ft Alexis y Fido', '3', 'Reggaeton', 8, 'music/vilchez/Aceite En La Cintura.mp3', 7774437, 'vilchez'),
(15, 'To'' Sieguito', 'Luis Fonsi ft Daddy Yankke', '3', 'Reggaeton', 9, 'music/vilchez/To'' sieguito.mp3', 3741308, 'vilchez'),
(16, 'The Chain', 'Fleetwood Mac', '4:30', 'Rock', 8, 'music/raul/The Chain - Fleetwood Mac.mp3', 4309287, 'raul'),
(17, 'Feel Good Inc', 'Gorillaz', '3:43', 'Rock Alternativo', 8, 'music/raul/Feel good inc-Gorillaz.mp3', 3437295, 'raul'),
(18, 'Young And Beautiful', 'Lana Del Rey', '3:56', 'Pop barroco', 8, 'music\\raul\\Lana Del Rey - Young And Beautiful.mp3', 9942269, 'raul'),
(19, 'Rock''n Roll Train', 'AC/DC', '4:22', 'Rock and roll', 9, 'music/angelr/ACDC-Rock Roll Train.mp3', 4193901, 'angelr'),
(21, 'Lose Yourself', 'Eminem', '5:23', 'Rap', 9, 'music/angelr/Eminem-Lose Yourself.mp3', 5172618, 'angelr'),
(22, 'Sinnerman', 'Nina Simone', '4', 'House', 5, 'music/francis/Sinnerman.mp3', 8820420, 'francis'),
(23, 'Don''t Be so Shy', 'Imany', '3', 'House', 5, 'music/francis/Don''t Be so Shy.mp3', 7611018, 'francis'),
(24, 'Creci En Los Ochenta', 'El Reno Renardo', '5:27', 'Rock', 5, 'music/joseluis/El Reno Renardo - 08 - Creci En Los Ochenta.mp3', 7987200, 'joseluis'),
(26, 'Turbo Lover', 'Judas Priest', '5:31', 'Rock Heavy', 4, 'music/joseluis/01 Turbo Lover.mp3', 5301537, 'joseluis'),
(27, 'Im Gonna Be', 'The Proclaimers', '3:35', 'Rock Clásico', 4, 'music/joseluis/The Proclaimers - Im Gonna Be.mp3', 5185536, 'joseluis'),
(28, 'Daft Punk', 'Pentatonix', '4:09', 'Pop', 10, 'music/carlos/Daft Punk.mp3', 9998093, 'carlos'),
(29, 'Starboy', 'The Weekend', '3:50', 'R&B', 7, 'music/carlos/Starboy.mp3', 9381680, 'carlos'),
(30, 'Human', 'Rag''n''Bone Man', '3:20', 'Rock alternativo', 8, 'music/carlos/Human.mp3', 8119695, 'carlos'),
(31, 'Father And Son', 'Cat Stevens', '3:52', 'Pop', 9, 'music/tarek/fathersoon.mp3', 3722084, 'tarek'),
(34, 'ThePride', 'Five Finger Death Punch', '4:02', 'Heavy', 8, 'music/nacho/ThePride.mp3', 3877565, 'nacho'),
(35, 'We''re All To Blame', 'Sum 41', '3:54', 'Heavy', 9, 'music/nacho/We''re All To Blame.mp3', 3750379, 'nacho'),
(37, 'AmericanJesus', 'Bad Religion', '3:25', 'Punk Rock', 8, 'music/nacho/AmericanJesus.mp3', 3273906, 'nacho'),
(38, 'Banana Pancakes', 'Jack Johnson', '3:30', 'Surf', 3, '/music/angelh/Jack Johnson - banana pancakes.mp3', 24455555, 'angelh'),
(39, 'Drain You', 'Nirvana', '3:40', 'Rock', 5, 'music/angelh/Nirvana - Drain You.mp3', 23423423, 'angelh'),
(46, 'Chattahoochee', 'Alan Jackson', '04:05', 'Country', 7, 'music/gabriel/Alan Jackson - Chattahoochee.mp3', 3918817, 'gabriel'),
(47, 'Hurt', 'Johnny Cash', '3:49', 'Country', 8, 'music/gabriel/Johnny Cash - Hurt HD 720p.mp3', 3679452, 'gabriel'),
(48, 'Metal Gear Solid Alert (!)', 'Konami', '0:01', 'sonido', 10, 'music/raul/Metal Gear Solid Alert (!).mp3', 42142, 'raul'),
(49, 'Changes', '2-Pac', '4:35', 'Rap', 9.4, 'music/tarek/changes.mp3', 4435700, 'tarek');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `Id` int(11) NOT NULL,
  `Id_usuario` varchar(100) CHARACTER SET latin1 NOT NULL,
  `Id_cancion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`Id`, `Id_usuario`, `Id_cancion`) VALUES
(1, 'lolo', 5),
(2, 'lolo', 1),
(3, 'adil', 14),
(4, 'adil', 5),
(9, 'josed', 21),
(10, 'josed', 17),
(11, 'kiki', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Login` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `Password` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Login`, `Password`) VALUES
('adil', 'adil'),
('alejandro', 'alejandro'),
('angelh', 'angelh'),
('angelr', 'angelr'),
('antonio', 'antonio'),
('carlos', 'carlos'),
('fran', 'fran'),
('francis', 'francis'),
('gabriel', 'gabriel'),
('jaime', 'jaime'),
('josed', 'josed'),
('joseluis', 'joseluis'),
('kiki', 'kiki'),
('lolo', 'lolo'),
('nacho', 'nacho'),
('raul', 'raul'),
('tarek', 'tarek'),
('vero', 'vero'),
('vilchez', 'vilchez');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cancion`
--
ALTER TABLE `cancion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Login`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cancion`
--
ALTER TABLE `cancion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
